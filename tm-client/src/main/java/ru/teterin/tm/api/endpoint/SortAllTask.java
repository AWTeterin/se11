package ru.teterin.tm.api.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for sortAllTask complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="sortAllTask"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="session" type="{http://endpoint.api.tm.teterin.ru/}session" minOccurs="0"/&gt;
 *         &lt;element name="sortOption" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sortAllTask", propOrder = {
    "session",
    "sortOption"
})
public class SortAllTask {

    protected Session session;
    protected String sortOption;

    /**
     * Gets the value of the session property.
     *
     * @return possible object is
     * {@link Session }
     */
    public Session getSession() {
        return session;
    }

    /**
     * Sets the value of the session property.
     *
     * @param value allowed object is
     *              {@link Session }
     */
    public void setSession(Session value) {
        this.session = value;
    }

    /**
     * Gets the value of the sortOption property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getSortOption() {
        return sortOption;
    }

    /**
     * Sets the value of the sortOption property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setSortOption(String value) {
        this.sortOption = value;
    }

}

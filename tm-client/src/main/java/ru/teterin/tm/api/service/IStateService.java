package ru.teterin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.endpoint.Session;
import ru.teterin.tm.command.AbstractCommand;

import java.util.Map;

public interface IStateService {

    @Nullable
    public Session getSession();

    public void setSession(
        @Nullable final Session session
    );

    @NotNull
    public AbstractCommand getCommand(
        @Nullable final String commandName
    );

    public void registryCommand(
        @NotNull final AbstractCommand command
    );

    @NotNull
    public Map<String, AbstractCommand> getCommands();

}

package ru.teterin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.endpoint.Session;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

public final class UserLogOutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-logout";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Log out to the task manager.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.USER_LOGOUT);
        @Nullable final Session session = stateService.getSession();
        if (session != null) {
            sessionEndpoint = serviceLocator.getSessionEndpoint();
            sessionEndpoint.closeSession(session);
        }
        stateService.setSession(null);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}

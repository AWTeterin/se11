package ru.teterin.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Information about building the app.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.ABOUT);
        terminalService.print(Constant.CREATED_BY + " " + Manifests.read(Constant.CREATED_BY));
        terminalService.print(Constant.BUILD_JDK + " " + Manifests.read(Constant.BUILD_NUMBER));
        terminalService.print(Constant.BUILD_NUMBER + " " + Manifests.read(Constant.BUILD_NUMBER));
        terminalService.print(Constant.DEVELOPER + " " + Manifests.read(Constant.DEVELOPER));
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return false;
    }

}

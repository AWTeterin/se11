package ru.teterin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.endpoint.Project;
import ru.teterin.tm.api.endpoint.Session;
import ru.teterin.tm.api.endpoint.Status;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

import javax.xml.datatype.XMLGregorianCalendar;

public final class ProjectEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Choose project by id and edit it.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.PROJECT_EDIT);
        @Nullable final Session session = stateService.getSession();
        if (session == null) {
            throw new IllegalArgumentException(Constant.INCORRECT_COMMAND);
        }
        terminalService.print(Constant.ENTER_PROJECT_ID);
        @Nullable final String id = terminalService.readString();
        if (id == null || id.isEmpty()) {
            throw new IllegalArgumentException(Constant.EMPTY_ID);
        }
        projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final Project project = projectEndpoint.findOneProject(session, id);
        terminalService.print(project);
        @NotNull final Project result = terminalService.readProject();
        @NotNull final XMLGregorianCalendar dateCreate = project.getDateCreate();
        result.setDateCreate(dateCreate);
        @Nullable final String userId = session.getUserId();
        result.setUserId(userId);
        result.setId(id);
        terminalService.print(Constant.ENTER_STATUS);
        @Nullable final String strStatus = terminalService.readString();
        if (Constant.READY.equals(strStatus)) {
            result.setStatus(Status.READY);
        }
        if (Constant.IN_PROGRESS.equals(strStatus)) {
            result.setStatus(Status.IN_PROGRESS);
        }
        if (Constant.PLANNED.equals(strStatus)) {
            result.setStatus(Status.PLANNED);
        }
        projectEndpoint.mergeProject(session, result);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}

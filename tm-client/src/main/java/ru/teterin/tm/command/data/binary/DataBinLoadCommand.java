package ru.teterin.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.endpoint.Role;
import ru.teterin.tm.api.endpoint.Session;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

public final class DataBinLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-bin-load";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "The load of the subject area with the use of serialization.";
    }

    @Override
    public void execute() throws Exception {
        terminalService.print(Constant.DATA_BIN_LOAD);
        @Nullable final Session session = stateService.getSession();
        if (session == null) {
            throw new IllegalArgumentException(Constant.INCORRECT_COMMAND);
        }
        domainEndpoint = serviceLocator.getDomainEndpoint();
        domainEndpoint.loadBinary(session);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public Role[] roles() {
        @NotNull final Role[] onlyAdmin = new Role[]{Role.ADMIN};
        return onlyAdmin;
    }

}

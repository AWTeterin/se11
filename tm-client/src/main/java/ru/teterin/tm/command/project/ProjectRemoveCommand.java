package ru.teterin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.endpoint.Session;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

public final class ProjectRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove project by ID.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.PROJECT_REMOVE);
        @Nullable final Session session = stateService.getSession();
        if (session == null) {
            throw new IllegalArgumentException(Constant.INCORRECT_COMMAND);
        }
        terminalService.print(Constant.ENTER_PROJECT_ID);
        @Nullable final String projectId = terminalService.readString();
        if (projectId == null || projectId.isEmpty()) {
            throw new IllegalArgumentException(Constant.EMPTY_ID);
        }
        projectEndpoint = serviceLocator.getProjectEndpoint();
        projectEndpoint.removeProject(session, projectId);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}

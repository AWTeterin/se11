package ru.teterin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.endpoint.Session;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

public final class UserLogInCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-login";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Log in to the task manager.";
    }

    @Override
    public void execute() {
        terminalService.print(Constant.USER_LOGIN);
        terminalService.print(Constant.ENTER_LOGIN);
        @Nullable final String login = terminalService.readString();
        terminalService.print(Constant.ENTER_PASSWORD);
        @Nullable final String password = terminalService.readString();
        sessionEndpoint = serviceLocator.getSessionEndpoint();
        @Nullable Session session = stateService.getSession();
        if (session != null) {
            sessionEndpoint.closeSession(session);
        }
        session = sessionEndpoint.openSession(login, password);
        if (session == null) {
            throw new IllegalArgumentException(Constant.INCORRECT_LOGIN_OR_PASSWORD);
        }
        stateService.setSession(session);
        @NotNull final String helloMessage = Constant.HELLO + login;
        terminalService.print(helloMessage);
    }

    @Override
    public boolean secure() {
        return false;
    }

}

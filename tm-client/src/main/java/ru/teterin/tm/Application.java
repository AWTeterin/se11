package ru.teterin.tm;

import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.context.Bootstrap;

public final class Application {

    public static void main(String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }

}

package ru.teterin.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.constant.Constant;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;

public final class DateUuidParseUtil {

    @NotNull
    public static String isUuid(
        @NotNull final String id
    ) {
        try {
            UUID.fromString(id);
            return id;
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(Constant.INCORRECT_ID);
        }
    }

    @NotNull
    public static XMLGregorianCalendar isDate(
        @NotNull final String stringDate
    ) {
        try {
            @NotNull final Date inputDate = Constant.DATE_FORMAT.parse(stringDate);
            @NotNull final GregorianCalendar temp = new GregorianCalendar();
            temp.setTime(inputDate);
            @NotNull final XMLGregorianCalendar result = DatatypeFactory.newInstance().newXMLGregorianCalendar(temp);
            return result;
        } catch (ParseException | DatatypeConfigurationException e) {
            throw new IllegalArgumentException(Constant.INCORRECT_DATE);
        }
    }

    @NotNull
    public static String xmlDateToString(
        @NotNull final XMLGregorianCalendar date
    ) {
        @NotNull final DateFormat formatter = Constant.DATE_FORMAT;
        @NotNull final GregorianCalendar temp = date.toGregorianCalendar();
        @NotNull final Date dateObject = temp.getTime();
        @NotNull final String result = formatter.format(dateObject);
        return result;
    }

}

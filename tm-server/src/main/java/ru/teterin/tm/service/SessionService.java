package ru.teterin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.repository.ISessionRepository;
import ru.teterin.tm.api.repository.IUserRepository;
import ru.teterin.tm.api.service.IPropertyService;
import ru.teterin.tm.api.service.ISessionService;
import ru.teterin.tm.entity.Session;
import ru.teterin.tm.entity.User;
import ru.teterin.tm.enumerated.Role;
import ru.teterin.tm.exception.AccessForbiddenException;
import ru.teterin.tm.util.PasswordHashUtil;
import ru.teterin.tm.util.SignatureUtil;

public final class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final ISessionRepository sessionRepository;

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final IPropertyService propertyService;

    public SessionService(
        @NotNull final ISessionRepository sessionRepository,
        @NotNull final IUserRepository userRepository,
        @NotNull final IPropertyService propertyService
    ) {
        super(sessionRepository);
        this.sessionRepository = sessionRepository;
        this.userRepository = userRepository;
        this.propertyService = propertyService;
    }

    @Nullable
    @Override
    public Session open(
        @Nullable final String login,
        @Nullable final String password
    ) {
        final boolean noLogin = login == null || login.isEmpty();
        if (noLogin) {
            return null;
        }
        final boolean noPassword = password == null || password.isEmpty();
        if (noPassword) {
            return null;
        }
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) {
            return null;
        }
        @Nullable final String hashPassword = PasswordHashUtil.md5(password);
        if (hashPassword == null) {
            return null;
        }
        @NotNull final String userPassword = user.getPassword();
        final boolean incorrectPassword = !hashPassword.equals(userPassword);
        if (incorrectPassword) {
            return null;
        }
        @Nullable final Session session = new Session();
        @NotNull final String userId = user.getId();
        session.setUserId(userId);
        @Nullable final Role userRole = user.getRole();
        session.setRole(userRole);
        sign(session);
        sessionRepository.persist(session);
        return session;
    }

    @Override
    public void validate(
        @Nullable final Session session
    ) {
        if (session == null) {
            throw new AccessForbiddenException();
        }
        @Nullable final String signature = session.getSignature();
        final boolean noSignature = signature == null || signature.isEmpty();
        if (noSignature) {
            throw new AccessForbiddenException();
        }
        @Nullable final String userId = session.getUserId();
        final boolean noUserId = userId == null || userId.isEmpty();
        if (noUserId) {
            throw new AccessForbiddenException();
        }
        @NotNull final Session temp = session.clone();
        if (temp == null) {
            throw new AccessForbiddenException();
        }
        @NotNull final String signatureSource = session.getSignature();
        sign(temp);
        @Nullable final String signatureTarget = temp.getSignature();
        final boolean incorrectSignature = !signatureSource.equals(signatureTarget);
        if (incorrectSignature) {
            throw new AccessForbiddenException();
        }
        @NotNull final String sessionId = session.getId();
        final boolean incorrectId = !repository.contains(sessionId);
        if (incorrectId) {
            throw new AccessForbiddenException();
        }
    }

    @Override
    public void validateWithRole(
        @Nullable final Session session,
        @Nullable final Role role
    ) {
        validate(session);
        @NotNull final Role userRole = session.getRole();
        final boolean accessDenied = !userRole.equals(role);
        if (accessDenied) {
            throw new AccessForbiddenException();
        }
    }

    @Nullable
    @Override
    public Session close(
        @Nullable final Session session
    ) {
        validate(session);
        @NotNull final String sessionId = session.getId();
        @Nullable final Session result = sessionRepository.remove(sessionId);
        return result;
    }

    @Nullable
    @Override
    public Session sign(
        @Nullable final Session session
    ) {
        if (session == null) {
            return null;
        }
        session.setSignature(null);
        @NotNull final String salt = propertyService.getSessionSalt();
        @NotNull final Integer cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        return session;
    }

}

package ru.teterin.tm.entity;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@JsonAutoDetect
@XmlRootElement
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Domain implements Serializable {

    public static final long serialVersionUID = 4;

    @Nullable
    @XmlElementWrapper(name = "users")
    private List<User> users;

    @Nullable
    @XmlElementWrapper(name = "tasks")
    private List<Task> tasks;

    @Nullable
    @XmlElementWrapper(name = "projects")
    private List<Project> projects;

}


package ru.teterin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.repository.ISessionRepository;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.entity.Session;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    public void validate(
        @NotNull final String userId,
        @NotNull final String sessionId
    ) throws IllegalArgumentException {
        @Nullable final Session session = findOne(sessionId);
        if (session == null) {
            throw new IllegalArgumentException(Constant.NO_SESSION);
        }
        @NotNull final String sessionUserId = session.getUserId();
        final boolean noSession = !userId.equals(sessionUserId);
        if (noSession) {
            throw new IllegalArgumentException(Constant.NO_SESSION);
        }
    }

}

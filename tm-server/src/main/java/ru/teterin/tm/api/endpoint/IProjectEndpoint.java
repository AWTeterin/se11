package ru.teterin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.Project;
import ru.teterin.tm.entity.Session;
import ru.teterin.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @WebMethod
    public void mergeProject(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "project") @Nullable final Project project
    );

    @WebMethod
    public void persistProject(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "project") @Nullable final Project project
    );

    @NotNull
    @WebMethod
    public Project findOneProject(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "projectId") @Nullable final String projectId
    );

    @NotNull
    @WebMethod
    public List<Project> findAllProject(
        @WebParam(name = "session") @Nullable final Session session
    );

    @WebMethod
    public void removeProject(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "projectId") @Nullable final String projectId
    );

    @WebMethod
    public void removeAllProjects(
        @WebParam(name = "session") @Nullable final Session session
    );

    @NotNull
    @WebMethod
    public Collection<Project> searchProjectByString(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "string") @Nullable final String string
    );

    @NotNull
    @WebMethod
    public Collection<Project> sortAllProject(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "sortOption") @Nullable final String sortOption
    );

    @NotNull
    @WebMethod
    public Collection<Task> findAllProjectTasks(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "projectId") @Nullable final String projectId
    );

}

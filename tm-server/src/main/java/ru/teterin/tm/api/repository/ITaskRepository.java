package ru.teterin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.entity.Task;

import java.util.Collection;
import java.util.Comparator;

public interface ITaskRepository extends IRepository<Task> {

    public void removeAllByProjectId(
        @NotNull final String userId,
        @NotNull final String projectId
    );

    @NotNull
    public Collection<Task> findAllByProjectId(
        @NotNull final String userId,
        @NotNull final String projectId
    );

    @NotNull
    public Collection<Task> findAndSortAll(
        @NotNull final String userId,
        @NotNull final Comparator<Task> comparator
    );

    @NotNull
    public Collection<Task> searchByString(
        @NotNull final String userId,
        @NotNull final String searchString
    );

}

package ru.teterin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.entity.Project;

import java.util.Collection;
import java.util.Comparator;

public interface IProjectRepository extends IRepository<Project> {

    @NotNull
    public Collection<Project> findAndSortAll(
        @NotNull final String userId,
        @NotNull final Comparator<Project> comparator
    );

    @NotNull
    public Collection<Project> searchByString(
        @NotNull final String userId,
        @NotNull final String searchString
    );

}

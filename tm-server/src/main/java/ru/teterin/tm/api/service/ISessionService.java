package ru.teterin.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.Session;
import ru.teterin.tm.enumerated.Role;

public interface ISessionService extends IService<Session> {

    @Nullable
    public Session open(
        @Nullable final String login,
        @Nullable final String password
    );

    public void validate(
        @Nullable final Session session
    );

    public void validateWithRole(
        @Nullable final Session session,
        @Nullable final Role role
    );

    @Nullable
    public Session close(
        @Nullable final Session session
    );

    @Nullable
    public Session sign(
        @Nullable final Session session
    );

}

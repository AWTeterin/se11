package ru.teterin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.Domain;

import java.io.IOException;
import java.nio.file.Path;

public interface IDomainService {

    public void save(
        @Nullable final Domain domain
    );

    public void load(
        @Nullable final Domain domain
    );

    public void saveBinary() throws IOException;

    public void loadBinary() throws IOException;

    public void saveJsonFasterxml() throws IOException;

    public void saveJsonJaxb() throws Exception;

    public void loadJsonFasterxml() throws IOException;

    public void loadJsonJaxb() throws Exception;

    public void saveXmlFasterxml() throws IOException;

    public void saveXmlJaxb() throws Exception;

    public void loadXmlFasterxml() throws IOException;

    public void loadXmlJaxb() throws Exception;

    public void clearData();

    @NotNull
    public Domain readData(
        @NotNull final Path path
    ) throws IOException;

}

package ru.teterin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.Session;
import ru.teterin.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @WebMethod
    public void mergeTask(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "task") @Nullable final Task task
    );

    @WebMethod
    public void persistTask(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "task") @Nullable final Task task
    );

    @NotNull
    @WebMethod
    public Task findOneTask(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "taskId") @Nullable final String taskId
    );

    @NotNull
    @WebMethod
    public List<Task> findAllTask(
        @WebParam(name = "session") @Nullable final Session session
    );

    @WebMethod
    public void removeTask(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "taskId") @Nullable final String taskId
    );

    @WebMethod
    public void removeAllTasks(
        @WebParam(name = "session") @Nullable final Session session
    );

    @NotNull
    @WebMethod
    public Collection<Task> searchTaskByString(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "string") @Nullable final String string
    );

    @NotNull
    @WebMethod
    public Collection<Task> sortAllTask(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "sortOption") @Nullable final String sortOption
    );

    @WebMethod
    public void linkTask(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "projectId") @Nullable final String projectId,
        @WebParam(name = "taskId") @Nullable final String taskId
    );

}

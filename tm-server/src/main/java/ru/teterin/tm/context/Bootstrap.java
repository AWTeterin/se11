package ru.teterin.tm.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.endpoint.*;
import ru.teterin.tm.api.repository.IProjectRepository;
import ru.teterin.tm.api.repository.ISessionRepository;
import ru.teterin.tm.api.repository.ITaskRepository;
import ru.teterin.tm.api.repository.IUserRepository;
import ru.teterin.tm.api.service.*;
import ru.teterin.tm.endpoint.*;
import ru.teterin.tm.entity.User;
import ru.teterin.tm.enumerated.Role;
import ru.teterin.tm.repository.ProjectRepository;
import ru.teterin.tm.repository.SessionRepository;
import ru.teterin.tm.repository.TaskRepository;
import ru.teterin.tm.repository.UserRepository;
import ru.teterin.tm.service.*;
import ru.teterin.tm.util.PasswordHashUtil;

import javax.xml.ws.Endpoint;

@Getter
@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository, projectRepository);

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository, taskRepository);

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IDomainService domainService = new DomainService(userService, projectService, taskService);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository, userRepository, propertyService);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    public void init() {
        initUser();
        registry(sessionEndpoint);
        registry(userEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(domainEndpoint);
    }

    private void initUser() {
        @NotNull final User user = new User();
        user.setId("00000000-0000-0000-0000-000000000100");
        user.setUserId("00000000-0000-0000-0000-000000000100");
        user.setLogin("user");
        user.setPassword(PasswordHashUtil.md5("user"));
        user.setRole(Role.USER);
        @NotNull final User admin = new User();
        admin.setId("00000000-0000-0000-0000-000000000111");
        admin.setUserId("00000000-0000-0000-0000-000000000111");
        admin.setLogin("admin");
        admin.setPassword(PasswordHashUtil.md5("admin"));
        admin.setRole(Role.ADMIN);
        userRepository.persist(user.getId(), user);
        userRepository.persist(admin.getId(), admin);
    }

    private void registry(final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

}
